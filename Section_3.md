Section 3 : Game Officials

### 3.1 Officials
- 3.1.1 A tournament requires a Head Referee designated by the event organisers and communicated to the players at the beginning of the tournament.
- 3.1.2 A game requires the following officials:
        ‐ Main Referee
        ‐ Goal Judges (2)
        ‐ Game Assistant
        ‐ Assistant Referee
    - 3.1.2.1 One Referee is designated as the Main Referee to rule the game and is communicated to each team prior to the start of the game.
    - 3.1.2.2 Goal Judges are appointed and approved by the Main Referee prior to the start of the game.
      - 3.1.2.2.1 Goal Judges are visually distinguishable from spectators.
    - 3.1.2.3 Game Assistants are not mandatory, but encouraged.
    - 3.1.2.3.1 In case Game Assistants are not available, the Main Referee will assume their responsibilities and duties.

### 3.2 Equipment
- 3.2.1 Event organisers must provide the following equipment for the Game Officials:
    ‐ Stopwatch
    ‐ Whistle
    ‐ Scoreboard
    ‐ Flags, hats or vests (2) for Goal Judges
    ‐ Pen and paper
    ‐ Bike check stickers, tape, or similar markers
    ‐ Vests for players
        
### 3.3 Positioning
- 3.3.1 Main Referee
    - 3.3.1.1 The Main Referee and Game Assistants must be positioned at half court, with a direct unobstructed view of the court, preferably at a height.
- 3.3.2 Game Assistants
    - 3.3.2.1 Game Assistants must be positioned close to the Main Referee, at a distance where communication is favorable and easy.
- 3.3.3 Assistant Referees
    - 3.3.3.1 Assistant Referees may position themselves across the court to help signal actions otherwise difficult to see by the Main Referee; otherwise they will take position right next to the Main Referee in order to follow the action on and off ball according to the needs of the game.
- 3.3.4 Goal Judges
    - 3.3.4.1 Goal Judges must be positioned on the opposite side across from the Main Referee, close to the goal line slightly towards mid court, with a clear and unobstructed view of the goal opening; preferably at a height.
        - 3.3.4.1.1 In case that positioning is unavailable, they must be placed behind the goal or slightly to the side of it, depending on where they obtain a better view of the goal opening and approved by the Main Referee.


### 3.4 Duties
- 3.4.1 Head Referee
    - 3.4.1.1 The Head Referee oversees the proper enforcement of the ruleset by the other Officials.
        - 3.4.1.1.1 The Head Referee cannot override a Main Referee’s decision during a game.
    - 3.4.1.2 The Head Referee must provide a copy of the current ruleset for reference.
        - 3.4.1.2.1 It is the responsibility of the Head Referee to explain the ruleset to players and other Game Officials.
    - 3.4.1.3 The Head Referee inspects the equipment of the players prior to the beginning of the event to determine its safety and conformity as per 2.3.
    - 3.4.1.3.1 The Head Referee might obtain help from other Officials to verify the equipment.
    - 3.4.1.4 The final score for forfeited games is defined and announced by the Head Referee before the beginning of the tournament.

- 3.4.2 Main Referee
    - 3.4.2.1 The Main Referee maintains full control of the game, enforcing the ruleset to the best of their ability.
        - 3.4.2.1.1 It is the responsibility of the Main Referee to assess the severity of all infractions and issue any penalty listed in Section 7 so that the competitive disadvantage is rectified.
    - 3.4.2.2 The Main Referee may inspect the equipment of the players prior to the game and proceed as per 5.12 if the equipment is deemed unsafe.
    - 3.4.2.3 The Main Referee signals the beginning and end of the game according to 5.1 and 5.5.
    - 3.4.2.4 The Main Referee signals all stoppages and starts of play according to 5.3 and 5.4.
    - 3.4.2.5 The Main Referee signals all infractions according to Section 6.

- 3.4.3 Goal Judges
    - 3.4.3.1 Goal Judges must signal goals scored on their side of the court according to 3.5.2.1.
    - 3.4.3.2 Goal Judges should signal what they perceive to be a penalty in their close proximity, as per 3.5.2.3.
    - 3.4.3.2.1 A Goal Judge must be prepared to provide perspective to the Main Referee regarding any potential scores or penalties.
    - 3.4.3.2.2 Interaction with the Main Referee is strictly consultative and the final call must be made by the Main Referee.
    - 3.4.3.3 Goal Judges must return the goals to their position if moved or toppled during play.
    - 3.4.3.3.1 If the court does not allow access for the Goal Judge, the Main Referee must delegate this responsibility to another volunteer.
    - 3.4.3.3.2 If no access to the court is available, players are instructed that they are responsible for returning the goal to the correct position.
    - 3.4.3.4 Goal Judges must signal as per 3.5.2.4 false starts at the joust as per 5.2.1.1.
    - 3.4.3.5 Goal Judges may signal a timeout otherwise unheard by the Main Referee, Game Assistants or Assistant Referee

- 3.4.4 Game Assistant
    - 3.4.4.1 The Game Assistant keeps the time of the game and is responsible for starting and stopping the official stopwatch.
    - 3.4.4.2 The Game Assistant times any penalties issued on players that result on the player being temporarily excluded from play.
        - 3.4.4.2.1 The Game Assistant is responsible for letting temporarily excluded players know when they may return to play.
    - 3.4.4.3 The Game Assistant maintains the game log on paper.
    - 3.4.4.4 The game log must contain the following:
            ‐ Goals per team.
            ‐ Penalties by player.
        - 3.4.4.4.1 A tournament organiser might additionally request:
            ‐ Time of all goals and penalties called.
            ‐ Name of scoring player.
    - 3.4.4.5 If there is a timer and scoreboard visible to players, the Game Assistant is responsible for starting, stopping the time and adding goals to it.
    - 3.4.4.6 The Game Assistant calls out game‐times at periodic intervals, as well as the following:
            ‐ After goals.
            ‐ Half‐time.
            ‐ Restart of play.
            ‐ Two (2) minutes from the end.
            ‐ By player’s request.
        - 3.4.4.6.1 After the two minute warning, the Game Assistant must call out sixty seconds, thirty seconds, ten seconds and a countdown from five seconds to one.

- 3.4.5 Assistant Referee
    - 3.4.5.1 Assistant Referees should signal what they perceive to be a penalty according to
    - 3.5.3.1.
        - 3.4.5.1.1 Assistant Referees must be prepared to provide perspective to the Main Referee regarding any potential scores or penalties.
        - 3.4.5.1.2 Interaction with the Main Referee is strictly consultative and the final call must be made by the Main Referee.
    - 3.4.5.2 Assistant Referee can signal a timeout as per 3.5.3.2, otherwise unheard by the Main Referee or the Game Assistant


**3.5 Hand Signals**

- 3.5.1 Main Referee
    - 3.5.1.1 Start of play – Raises an arm in the air, dropping the arm as the whistle is blown.
    - 3.5.1.2 Restart of play – Extends an arm outward with the palm facing up to indicate play can begin.
    - 3.5.1.3 Delayed Penalty/Advantage – Raises an arm straight into the air.
    - 3.5.1.4 Possession after stoppage – Extends an arm in the direction of the team who restarts with the ball.
    
- 3.5.2 Goal Judges
    - 3.5.2.1 Goal scored – Raises an arm straight in the air.
        - 3.5.2.1.1 If a flag is available, the goal Judge raises the flag.
        - 3.5.2.2 Missed shot/Invalid goal – Crosses arms and extends them outwards in a sweeping motion.
        - 3.5.2.3 False Start/Infraction – Extends an arm straight in the air and the other pointing at the player that committed the infraction.
            - 3.5.2.3.1 If a flag is available, the goal Judge raises the flag and extends the other arm.
        - 3.5.2.4 Timeout – Makes a “T” shape with both arms.
        
- 3.5.3 Assistant Referee
    - 3.5.3.1 Infraction – Raises an arm in the air and the other pointing at the player that committed the infraction.
    - 3.5.3.2 Timeout – Makes a “T” shape with both arms.