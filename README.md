
![Logo](img/logo_ehba_300x300.png)

## 2019 RULESET 
#### Revision 190514
#### Sommaire

- [Section 1: Teams, Players & Equipment](Section_1.md)
- [Section 2: Game Facilities](Section_2.md)
- [Section 3: Game Officials](Section_3.md)
- [Section 4: Ball Play](Section_4.md)
- [Section 5: Game Mechanics](Section_5.md)
- [Section 6: Infractions](Section_6.md)
- [Section 7: Penalties](Section_7.md)
- [Section 8: Penalty Procedures](Section_8.md)
 
